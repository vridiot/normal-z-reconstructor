# Normal Z Reconstructor

Simple tool to reconstruct the Z (blue) channel in normal maps where only the XY (red green) are present.

## Requirements
numpy, PIL, argprse
