#!/usr/bin/env python3.9

import numpy as np
from PIL import Image
import argparse

parser = argparse.ArgumentParser("Process images to reconstruct normal Z channel from XY")
parser.add_argument("input", help="Input file without valid Z (blue) channel")
parser.add_argument("output", help="New output file with all 3 components")
args = parser.parse_args()

im2 = np.array(Image.open(args.input))
im2 = im2.astype('float32')


im2 /= 255.0

im2 *= 2
im2 -= 1

ir = im2[:,:,0]
ig = im2[:,:,1]
ib = im2[:,:,2]

dp = np.multiply(ir,ir)+np.multiply(ig,ig)

newb = np.sqrt(1 - np.clip(dp,0,1))

newb = np.dstack((ir,ig,newb))

newb += 1
newb /= 2

newb *= 255


pi = Image.fromarray(newb.astype(np.uint8))
pi.save(args.output)
